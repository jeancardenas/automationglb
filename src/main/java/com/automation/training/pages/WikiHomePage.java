package com.automation.training.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class WikiHomePage extends BasePage{

	public WikiHomePage(WebDriver driver, String url) {
		super(driver);
		driver.get(url);
	}

	@FindBy(id="searchInput")
	private WebElement searchInput;

	@FindBy(xpath="//*[@id='search-form']/fieldset/button")
	private WebElement searchButton;



	public ArticlePage buscar(String busqueda) {
		searchInput.sendKeys(busqueda);
		searchButton.click();
		return new ArticlePage(getDriver());
	}
}
