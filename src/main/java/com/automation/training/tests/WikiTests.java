package com.automation.training.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.automation.training.pages.ArticlePage;
import com.automation.training.pages.WikiHomePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WikiTests extends BaseTests{

	@DataProvider(name = "inputData")
	public static Object[][] inputData() {
		return new Object[][] {{"Java", "Java"}, {"php", "PHP"}, {"Python", "Python"}};
	}

	@BeforeMethod(alwaysRun = true)
	@Parameters({"url"})
	private void loadPage(String url){
		myDriver.getDriver().get(url);
	}

	@Test(dataProvider = "inputData")
	public void testWikiSearch(String input, String result) {
		WikiHomePage home = getWikiHomePage();
		ArticlePage articlePage = home.buscar(input);
		Assert.assertEquals(articlePage.getPageTitle(),result);
	}

	@AfterClass(alwaysRun = true)
		private void afterClass() {
		wikiHome.dispose();
	}

}
