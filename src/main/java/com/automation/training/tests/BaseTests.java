package com.automation.training.tests;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.automation.training.MyDriver;
import com.automation.training.pages.WikiHomePage;

public class BaseTests {
	
	MyDriver myDriver;

	public WikiHomePage wikiHome;

	@BeforeSuite(alwaysRun=true)
    @Parameters({"browser", "url"})
	public void beforeSuite(String browser, String url) {
		myDriver = new MyDriver(browser);
		wikiHome = new WikiHomePage(myDriver.getDriver(), url);
	}

	public WikiHomePage getWikiHomePage() {
		return wikiHome;
	}


}
